const express = require('express');
const router = express.Router();
const {users} = require("./store");

router.post("/create", (req,res) => {
    const user = req.body;
    const currentId = users.currentId+1;
    users.list[currentId] = user;
    users.currentId++;
    res.send({user: {...users.list[currentId],id:currentId}});
})

router.delete("/:id", (req,res) => {
    const userId = req.params.id;
    if(!users.list[userId]){
        res.status(500).send('Something broke!');
    }else{
    delete users.list[userId];
}
    res.send(users.list);
});

router.get("/:id", (req,res) => {
    const userId = req.params.id;
    const userData = users.list[userId];
    res.send({user: userData});
});

router.put("/:id", (req,res) => {
    const user = req.body;
    const userId = req.params.id;
    users.list[userId] = user;
    res.send(users);
});
module.exports = router;