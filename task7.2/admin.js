const express = require('express');
const router = express.Router();
const {admins} = require("./store");

router.post("/create", (req,res) => {
    const admin = req.body;
    const currentId = admins.currentId+1;
    admins.list[currentId] = admin;
    admins.currentId++;
    res.send({admin: {...admins.list[currentId],id:currentId}});
})

router.delete("/:id", (req,res) => {
    const adminId = req.params.id;
    if(!admins.list[adminId]){
        res.status(500).send('Something broke!');
    }else{
    delete admins.list[adminId];
}
    res.send(admins.list);
});

router.get("/:id", (req,res) => {
    const adminId = req.params.id;
    const adminData = admins.list[adminId];
    res.send({admin: adminData});
});

router.put("/:id", (req,res) => {
    const admin = req.body;
    const adminId = req.params.id;
    admins.list[adminId] = admin;
    res.send(admins);
});
module.exports = router;