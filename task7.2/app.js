const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const urlencodedParser = bodyParser.urlencoded({extended: true});
const store = require('./store');
const userRoutes = require('./user');
const adminRoutes = require('./admin');

app.use('/user/', urlencodedParser);
app.use('/user/', userRoutes);
app.use(bodyParser.json());
app.use('/admin/', adminRoutes);
app.listen(3000);