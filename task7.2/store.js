global.users = {
    list: {},
    currentId: 0
};

global.admins = {
    list: {},
    currentId: 0
}

module.exports  = {
    users: global.users,
    admins: global.admins
};