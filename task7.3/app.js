const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const someChanges = require('./fileWorker');
const urlencodedParser = bodyParser.urlencoded({extended: true});
const emitter = require('./emitter');
const fs = require("fs");


app.use(bodyParser.json());
app.use(urlencodedParser);
app.use('', someChanges);

emitter.on('fileEv', function(fileName){
    let readableStream = fs.createReadStream(fileName, "utf8");
    let writeableStream = fs.createWriteStream("newWriteSimple.json");
    readableStream.pipe(writeableStream);
});

app.listen(8000);