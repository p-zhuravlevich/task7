const fs = require("fs");
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer();
const emitter = require('./emitter');


router.post("/writeSimple", (req,res) => {
    console.log(req.body);
    fs.writeFile("writeSimple.json", JSON.stringify(req.body), 'utf8', (err) => {
        if(err) throw err;
        res.send(req.body);
    });
});

router.get("/readSimple/:fileName", (req,res) => {
    const fileName = req.params.fileName;
    fs.readFile(fileName, 'utf8', (err, data) => {
        if(err) throw err;
        res.send(data);
    });
});

router.post("/writeStream", (req,res) => {
    let writeableStream = fs.createWriteStream("writeSimple.json", 'utf8');
    let bodyData = JSON.stringify(req.body)
    writeableStream.write(bodyData);
    res.send(req.body);
});

router.get("/readStream/:fileName", (req,res) => {
    const fileName = req.params.fileName;
    let readableStream = fs.createReadStream(fileName, "utf8")
    readableStream.on("data", (chunk) => res.send(chunk))
});

router.put("/writeFile", upload.single('file'),(req,res) => {
    let fileName = req.file.originalname;    
    emitter.emit('fileEv', fileName);
    res.send(`<h1>Создание и запись нового файла прошла успешно!</h1>`);
});

module.exports = router;