const express = require("express");
const adminController = require("../controllers/adminController");
const adminRouter = express.Router();
const bodyParser = require('body-parser');
 
adminRouter.use("/create", bodyParser.json(), adminController.addAdmin);
adminRouter.use("/:id", bodyParser.json(), adminController.getAdmins);
adminRouter.use("/:id", bodyParser.json(), adminController.deleteAdmin);
adminRouter.use("/:id", bodyParser.json(), adminController.putAdmin);
 
module.exports = adminRouter;