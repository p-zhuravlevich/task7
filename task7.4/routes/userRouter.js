const express = require("express");
const userController = require("../controllers/userController");
const userRouter = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});
 
userRouter.use("/create", urlencodedParser, userController.addUser);
userRouter.use("/:id", urlencodedParser, userController.getUsers);
userRouter.use("/:id", urlencodedParser, userController.deleteUser);
userRouter.use("/:id", urlencodedParser, userController.putUser);
 
module.exports = userRouter;