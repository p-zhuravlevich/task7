const {admins} = require('../models/admin');


const addAdmin = function (req, res){
    const admin = req.body;
    const currentId = admins.currentId+1;
    admins.list[currentId] = admin;
    admins.currentId++;
    res.send({admin: {...admins.list[currentId],id:currentId}});
};

const getAdmins = function(req, res){
    const adminId = req.params.id;
    const adminData = admins.list[adminId];
    res.send({admin: adminData});
};

const deleteAdmin = (req, res) =>{
        const adminId = req.params.id;
    if(!admins.list[adminId]){
        res.status(500).send('Something broke!');
    }else{
    delete admins.list[adminId];
}
    res.send(admins);
}

const putAdmin = function(req,res){
    const admin = req.body;
    const adminId = req.params.id;
    admins.list[adminId] = admin;
    res.send(admins);
};

module.exports = {deleteAdmin, putAdmin, getAdmins, addAdmin}