const {users} = require('../models/user');


const addUser = function (req, res){
    const user = req.body;
    const currentId = users.currentId+1;
    users.list[currentId] = user;
    users.currentId++;
    res.send({user: {...users.list[currentId],id:currentId}});
};

const getUsers = function(req, res){
    const userId = req.params.id;
    const userData = users.list[userId];
    res.send({user: userData});
};

const deleteUser = (req, res) =>{
        const userId = req.params.id;
    if(!users.list[userId]){
        res.status(500).send('Something broke!');
    }else{
    delete users.list[userId];
}
    res.send(users);
}

const putUser = function(req,res){
    const user = req.body;
    const userId = req.params.id;
    users.list[userId] = user;
    res.send(users);
};

module.exports = {deleteUser, putUser, getUsers, addUser}