const express = require("express");
const app = express();

// http://localhost:3000/about?id=1&name=Jora&age=66&sex=male

global.gf = [];
app.get("/me", function(request, response){
    response.send({id:global.user.id, id:global.user.name});
    response.send("<p>id=" + (global.user.id || process.argv[2])+"</p><p>Имя=" + (global.user.userName || process.argv[3]) + "</p><p>Возраст=" + (global.user.age || process.argv[4]) +"</p><p>Пол=" + (global.user.sex || process.argv[5]) +"</p>");
});

app.use("/", function(request, response, next){
    if (process.argv[2]) {
        response.send(`<h1>Параметр 1 передан задан статично и доступен <a href="/me">здесь</a></h1>`);
    } else if (process.argv[3]){
        response.send(`<h1>Параметр 2 передан задан статично и доступен <a href="/me">здесь</a></h1>`);
    } else if (process.argv[4]){
        response.send(`<h1>Параметр 3 передан задан статично и доступен <a href="/me">здесь</a></h1>`);
    } else if (process.argv[5]){
        response.send(`<h1>Параметр 4 передан задан статично и доступен <a href="/me">здесь</a></h1>`);
    } else {
        next();
    }
});

app.get("/about", function(request, response){
    global.user.id = request.query.id;
    global.user.userName = request.query.name;
    global.user.age = request.query.age;
    global.user.sex = request.query.sex;
    response.send(`<h1>Информация о пользователе доступна <a href="/me">здесь</a></h1>`);
});
app.listen(3000);